#include <stdio.h>

int function (int x, int y)
{
    int r = x;
    r++;
    return r * y;
}

int main (int argc, char * argv[])
{
    int x = 7;
    int y = argc;

    int r = function (x, y);

    printf ("RETURN: %d\n", r);
}
