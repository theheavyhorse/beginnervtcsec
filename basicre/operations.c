#include <stdio.h>

void function (int a, int b)
{
    int x;
    int y;
    int z;
    int w;

    x = a + b;
    y = a - b;
    z = a * b;
    w = a / b;

    printf ("argc+5=%d\n", x);
    printf ("argc-5=%d\n", y);
    printf ("argc*5=%d\n", z);
    printf ("argc/5=%d\n", w);
}

int main(int argc, char * argv[])
{
    int x = 5;

    function (x, argc);
}
